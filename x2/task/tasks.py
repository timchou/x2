#coding=utf-8
from x2 import celery,app,model
import urllib2,urllib,datetime,time

URL="http://www.227777777.com/sina_callback"

@celery.task(name="x2.analyze")
def analyze_comment(token,expires,sina_id):
    client=model.weibo.APIClient(app_key=app.config['SINA_WEIBO_KEY'], app_secret=app.config['SINA_WEIBO_SECRET'], redirect_uri=URL)
    client.set_access_token(token , expires)
    
    comments_all=None
    newlist=[]
    comments={}
    PERPAGE_WEIBO=200
    page=1
   
    while True:
        try:
            comments_all=client.get.comments__timeline( count=PERPAGE_WEIBO,page=page)
        except model.weibo.APIError as apierr:
            print str(apierr)
            return -1
        except urllib2.HTTPError as err:
            print str(err)   # 记录HTTP状态码
            print err.read() # 同时记录错误代码
            return -1

        page = page + 1
        if 'comments' not in comments_all or not comments_all['comments']:
            print 'no more data'
            print comments_all
            break
        print "analyze:this round ,we got  :" + str(len(comments_all['comments'])) + ' ,previous_cursor=' + str(comments_all['previous_cursor']) + ',next_cursor='+str(comments_all['next_cursor']) + ',total=' + str(comments_all['total_number'])
        
        for c in comments_all['comments']:
            if 'deleted' in c['status'] and c['status']['deleted'] == '1':
                comments_all['comments'].remove(c)
                continue

            newc={}
            newc['created_at']=c['created_at']
            newc['text']=c['text']
            
            newc['user']={}
            newc['user']['id']=c['user']['idstr']
            newc['user']['profile_image_url']=c['user']['profile_image_url']
            newc['user']['name']=c['user']['name']

            newc['status']={}
            newc['status']['id']=c['status']['id']
            newc['status']['user']={}
            newc['status']['user']['id']=c['status']['user']['idstr']
            newc['status']['user']['profile_image_url']=c['status']['user']['profile_image_url']
            newc['status']['user']['name']=c['status']['user']['name']
            newc['status']['text']=c['status']['text']

            if 'bmiddle_pic' in c['status']:
                newc['status']['bmiddle_pic']=c['status']['bmiddle_pic']
                newc['status']['thumbnail_pic']=c['status']['thumbnail_pic']

            if 'reply_comment' in c:
                newc['reply_comment']={}
                newc['reply_comment']['user']={}
                newc['reply_comment']['user']['id']=c['reply_comment']['user']['idstr']
                newc['reply_comment']['user']['name']=c['reply_comment']['user']['name']
                newc['reply_comment']['user']['profile_image_url']=c['reply_comment']['user']['profile_image_url']
                newc['reply_comment']['text']=c['reply_comment']['text']
            
            target=None
            if newc['user']['id'] != str(sina_id):
                target=newc['user']
            elif newc['status']['user']['id'] != str(sina_id):
                target=newc['status']['user']
            elif 'reply_comment' in newc and newc['reply_comment']['user']['id'] != str(sina_id):
                target=newc['reply_comment']['user']
            else:
                continue

            if target['id'] not in comments:
                comments[target['id']]=[]
                #comments[target][0]存放目标用户的信息
                comments[target['id']].append(target)

            comments[target['id']].append(newc)
            comments_all['comments'].remove(c)

    res=[]
    for c in comments.keys():
        newc={
            'uid'               :comments[c][0]['id'] ,
            'profile_image_url' :comments[c][0]['profile_image_url'],
            'name'              :comments[c][0]['name'],
            'cnt'               :len(comments[c])-1
            }
        res.append(newc)
            
    from operator import itemgetter
    newlist = sorted(res, key=itemgetter('cnt')) 
    newlist.reverse()
    
    #model.home.save_top(str(sina_id),newlist)
    #model.home.save_comments(str(sina_id),comments)

    return {'total_number':comments_all['total_number'],'top':newlist,'comments':comments}



@celery.task(name="x2.get_comments")
def get_comments(token,expires,obj_name,page):
    client=model.weibo.APIClient(app_key=app.config['SINA_WEIBO_KEY'], app_secret=app.config['SINA_WEIBO_SECRET'], redirect_uri=URL)
    client.set_access_token(token , expires)

    PERPAGE_WEIBO=200
    comments=list()
    comments_all=None
    page=1
   
    while True:
        try:
            comments_all=client.get.comments__timeline( count=PERPAGE_WEIBO,page=page)
        except model.weibo.APIError as apierr:
            print str(apierr)
            return -1
        except urllib2.HTTPError as err:
            print str(err)   # 记录HTTP状态码
            print err.read() # 同时记录错误代码
            return -1

        page = page + 1
        if 'comments' not in comments_all or not comments_all['comments']:
            break
        
        print "comment:this round ,we got  :" + str(len(comments_all['comments'])) + ' ,previous_cursor=' + str(comments_all['previous_cursor']) + ',next_cursor='+str(comments_all['next_cursor']) + ',total=' + str(comments_all['total_number'])
    
        for c in comments_all['comments']:
            if 'deleted' in c['status'] and c['status']['deleted'] == '1':
                comments_all['comments'].remove(c)
                continue
            if obj_name in (c['status']['user']['name'],c['user']['name'],c['status']['user']['idstr'],c['user']['idstr']):

                newc={}
                newc['created_at']=c['created_at']

                newc['status']={}
                newc['status']['id']=c['status']['id']
                newc['status']['user']={}
                newc['status']['user']['id']=c['status']['user']['id']
                newc['status']['user']['profile_image_url']=c['status']['user']['profile_image_url']
                newc['status']['user']['name']=c['status']['user']['name']
                newc['status']['text']=c['status']['text']

                if 'bmiddle_pic' in c['status']:
                    newc['status']['bmiddle_pic']=c['status']['bmiddle_pic']
                    newc['status']['thumbnail_pic']=c['status']['thumbnail_pic']

                newc['user']={}
                newc['user']['id']=c['user']['id']
                newc['user']['name']=c['user']['name']
                newc['text']=c['text']

                if 'reply_comment' in c:
                    newc['reply_comment']={}
                    newc['reply_comment']['user']={}
                    newc['reply_comment']['user']['id']=c['reply_comment']['user']['id']

                    newc['reply_comment']['user']['name']=c['reply_comment']['user']['name']
                    newc['reply_comment']['text']=c['reply_comment']['text']
                
                comments.append(newc)
            comments_all['comments'].remove(c)

            #if len(comments) >= 18:
            #    #ids.rstrip(',')
            #    midss=None
            #    try:
            #        midss=client.get.statuses__querymid(id=ids,type=1,is_batch=1)
            #    except model.weibo.APIError as apierr:
            #        print str(apierr)
            #        return -1
            #    except urllib2.HTTPError as err:
            #        print str(err)   # 记录HTTP状态码
            #        print err.read() # 同时记录错误代码
            #        return -1
            #    ids=""

            #    mids=dict()
            #    for mid in midss:
            #        mid=dict(mid)
            #        key=mid.keys()[0]
            #        mids[key]=mid[key]
            #    for c in comments:
            #        if str(c['status']['id']) in mids:
            #            c['status']['oriid']=mids[str(c['status']['id'])]
            #        else:
            #            c['status']['oriid']='1234567'

    return {'comments':comments , 'total_number':comments_all['total_number'] , 'obj_name':obj_name}
