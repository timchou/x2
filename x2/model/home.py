#coding=utf-8
from x2 import app
from x2.config.pmongo import Mongodb
from flask import session,request,url_for
from x2.model import weibo
import urllib2,time

'''
_id
nick
email
password
token    :sina token
exp      :sina expire
create
last_login
'''

def get_coll(collection="user"):
    db=Mongodb()
    coll=db.get_coll(collection)
    return coll

def save_sina_access_token(r,userinfo):
    #保存token

    #如果token已存在,更新下expire和user信息
    #如果token不存在，插入user信息

    coll=get_coll()
    user=coll.find({'sid':r['uid']})

    if user.count() > 0:
        coll.update({'sid':r['uid']},{"$set":{'exp':r['expires_in'], 'token':r['access_token']}})
    else:
        simple_u={}
        simple_u['sid']=str(r['uid'])
        simple_u['avatar']=userinfo['profile_image_url']
        simple_u['name']=userinfo['name']
        simple_u['pwd']="hello123"
        simple_u['token']=r['access_token']
        simple_u['exp']=r['expires_in']
        coll.insert(simple_u)

    user=coll.find({'sid':r['uid']},{'id':1 , 'sid':1 , 'name':1 , 'avatar':1 , '':1})[0]
    return user 

@app.template_filter('midtourl')
def midtourl(mid):
    url=None
    try:
        client=weibo.APIClient(app_key=app.config['SINA_WEIBO_KEY'], app_secret=app.config['SINA_WEIBO_SECRET'], redirect_uri=url_for('home.sina_callback',_external=True))
        client.set_access_token(session['token'] , session['expires_in'])
        url=client.get.statuses__querymid(access_token=session['token'] ,id=mid,type=1)
    except weibo.APIError as apierr:
        print str(apierr)
    except urllib2.HTTPError as err:
        print str(err)   # 记录HTTP状态码
        print err.read() # 同时记录错误代码

    if 'mid' in url:
        return url['mid']
    else:
        return None

def storeRecord(obj_id , m_name , s_name , task_id):
    '''
    1:my comments
    2:our comments
    '''
    if obj_id and m_name and s_name and task_id:
        coll=get_coll("records")
        if s_name == '-':
            doc={'userId':obj_id,'m':m_name,'s':s_name,'task_id':task_id,'create':time.time(),'type':1}
        else:
            doc={'userId':obj_id,'m':m_name,'s':s_name,'task_id':task_id,'create':time.time(),'type':2}
        coll.insert(doc)

def get_records(obj_id=None):
    coll=get_coll('records')
    res=None
    if obj_id:
        res=coll.find({'userId':obj_id})
    else:
        res=coll.find()
    return res

def get_record_by_task_id(task_id):
    coll=get_coll('records')
    res=None
    if task_id:
        res=coll.find({'task_id':task_id})
    return res

def get_record_by_userId(userId,ttype=None):
    coll=get_coll('records')
    res=None
    if ttype:
        res=coll.find({'userId':userId,'type':ttype})
    else:
        res=coll.find({'userId':userId})
    return res

def save_comments(sid,comments):
    coll=get_coll('comments')
    cnt=coll.find({'sid':sid}).count()

    if cnt > 0:
        return False

    coll.insert({'sid':sid,'c':comments})
    return True 

def get_my_comments_from_db(target_id):
    if 'login_user' not in session:
        return False

    coll=get_coll('comments')
    res=coll.find({'sid':str(session['login_user']['id'])})[0]
    res['comments']=res['c'][target_id]
    #for key in res['c'].keys():
    #    if key != target_id:
    del res['c']
    del res['comments'][0]

    return res


def get_top(sid):
    coll=get_coll('comments')
    res=coll.find({'sid':sid})
    
    top=process_top(res['c'])
    return top

def process_top(comments):
    res=[]
    for c in comments.keys():
        newc={
            'uid'               :comments[c][0]['id'] ,
            'profile_image_url' :comments[c][0]['profile_image_url'],
            'name'              :comments[c][0]['name'],
            'cnt'               :len(comments[c])-1
            }   
        res.append(newc)
    
    from operator import itemgetter
    newlist = sorted(res, key=itemgetter('cnt'))
    return newlist

