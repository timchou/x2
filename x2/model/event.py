#coding=utf-8
from x2.config.pmongo import Mongodb
from bson.objectid import ObjectId
import time

def get_coll(collection="event"):
    db=Mongodb()
    coll=db.get_coll(collection)
    return coll

def save_event(doc):
    coll=get_coll()
    doc['create']=time.time()
    event_id=coll.insert(doc)
    return event_id

def get_event(event_id):
    event_id=ObjectId(event_id)
    coll=get_coll()
    res=coll.find({'_id':event_id})
    return res[0]

def get_my_event(userid=None):
    coll=get_coll()
    if userid:
        res=coll.find({'uid':userid})
    else:
        res=coll.find()
    return res

def add_guy_to_event(event_id,name):
    event_id=ObjectId(event_id)
    coll=get_coll()
    coll.update({'_id':event_id} , {'$push' : {'g' : name}})
