#coding=utf-8
from x2.config.pmongo import Mongodb

def get_coll(collection="user"):
    db=Mongodb()
    coll=db.get_coll(collection)
    return coll

def new_user(data):
    coll=get_coll()
    coll.insert(data)

def get_user_by_email(email):
    if not email:
        return None

    coll=get_coll()
    res=coll.find( {'email':email} )

    return res

def get_users():
    coll=get_coll()
    res=coll.find()
    return res


def get_user_by_userid(userid):
    if not userid:
        return None

    coll=get_coll()
    res=coll.find( {'_id':userid} )
    if res.count() > 0:
	return res[0]
    else:
	return None
