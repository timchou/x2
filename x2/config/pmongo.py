# -*- coding: utf-8 -*-
from x2 import app
import pymongo

class Mongodb:
    '''
    host
    port
    db
    coll
    '''

    def __init__(self ,host=app.config['MONGOALCHEMY_SERVER'] ,port=app.config['MONGOALCHEMY_PORT']):
        self.host=host
        self.port=port
        self.conn=pymongo.Connection(host,port)


    def get_coll(self ,collect ,db=app.config['MONGOALCHEMY_DATABASE']):
        self.db=self.conn[db]
        self.coll=self.db[collect]
        return self.coll

    @staticmethod
    def getdb(collection):
        host=app.config['MONGOALCHEMY_SERVER']
        port=app.config['MONGOALCHEMY_PORT']
        db=app.config['MONGOALCHEMY_DATABASE']

        conn=pymongo.Connection(host,port)
        db=conn[db]
        coll=db[collection]

        return coll
