#coding=utf-8 
from flask import Flask,url_for,session,redirect
from time import time
from flask.ext.celery import Celery

def create_app():
    return Flask("x2")

app = Flask(__name__)
celery = Celery(app)
app.config.from_pyfile('config/flask.ini')

import x2.view.home
import x2.view.user
import x2.view.event

app.register_blueprint(x2.view.home.mod)
app.register_blueprint(x2.view.user.mod)
app.register_blueprint(x2.view.event.mod)

@app.errorhandler(404)
def not_found(error):
    return "555,not found this page..<a href='"+url_for('home.index')+"'>return</a>", 404 

@app.errorhandler(500)
def server_error(error):
    return "555,server is crashed...<a href='"+url_for('home.index')+"'>return</a>", 404 

@app.before_request
def before_request():
    if 'expires_in' in session:
        x=session['expires_in'] - int(time())
        if x <= 120:
            session.pop('login_user',None)

@app.teardown_request
def teardown_request(exception):
    pass
