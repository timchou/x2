#coding=utf-8
from __future__ import division
from flask import Blueprint ,request ,render_template ,session ,redirect ,url_for,jsonify
from x2 import model,app,celery,task
import urllib2,urllib,datetime,time

mod=Blueprint('user' ,__name__)

@mod.route('/analyze_comment' ,methods=['GET','POST'])
def analyze_comment():
    if 'login_user' not in session:
        return redirect(url_for('home.index'))

    if request.method == 'GET':
        task_id=request.args.get('task_id','')
        
        #只能查看自己的
        t=model.home.get_record_by_task_id(task_id)
        if t and t.count() > 0:
            if t[0]['userId'] != session['login_user']['_id'] and session['login_user']['sid']!='1710252570':
                return redirect(url_for('home.error_page',code=1003))
        
        res=None
        total_number=None
        is_ready=None
        if task_id:
            is_ready=task.tasks.analyze_comment.AsyncResult(task_id).ready()
            if is_ready:
                res=task.tasks.analyze_comment.AsyncResult(task_id).get()
                if res == -1:
                    return redirect(url_for('home.error_page',code=1001))
                total_number=res['total_number']
                res=res['top']
        else:
            #检查上次task是否还未失效
            res=model.home.get_record_by_userId(session['login_user']['_id'],1)
            haveone=False
            for r in res:
                ltime=time.localtime(r['create'])
                if (int(time.time()) - int(time.mktime(ltime))) < 24*3600:
                    haveone=True
            if haveone:
                return redirect(url_for('home.error_page',code=1002))

            res=task.tasks.analyze_comment.apply_async((session['token'],session['expires_in'],session['login_user']['sid']))
            model.home.storeRecord(session['login_user']['_id'],session['login_user']['name'], '-' , res.task_id)
            return redirect(url_for('user.analyze_comment',task_id=res.task_id))

        return render_template('user/analyze.html',is_ready=is_ready,res=res,url=request.url,task_id=task_id,total_number=total_number)

@mod.route('/comment_with_you' ,methods=['GET','POST'])
def comment_with_you():
    if 'login_user' not in session:
        return redirect(url_for('user.login'))

    if request.method == 'POST':
        obj_name=request.form['obj_name']
        if obj_name:
            res=model.home.get_record_by_userId(session['login_user']['_id'],2)
            haveone=False
            for r in res:
                if r['s'] != obj_name:
                    continue

                ltime=time.localtime(r['create'])
                if (int(time.time()) - int(time.mktime(ltime))) < 24*3600:
                    haveone=True
            if haveone:
                return redirect(url_for('home.error_page',code=1002))

            res=task.tasks.get_comments.apply_async((session['token'],session['expires_in'],obj_name,1))
            model.home.storeRecord(session['login_user']['_id'],session['login_user']['name'], obj_name , res.task_id)
            return redirect(url_for('user.comment_with_you',task_id=res.task_id))

    if request.method == 'GET':
        #1.通过task_id查看
        task_id=request.args.get('task_id','')
        obj_name=request.args.get('obj_name','')

        #2.通过comments表查看
        target_id=request.args.get('target_id','')
        ttid=request.args.get('ttid','')

        #只能查看自己的
        t=model.home.get_record_by_task_id(task_id)
        if t and t.count() > 0:
            if t[0]['userId'] != session['login_user']['_id'] and session['login_user']['sid']!='1710252570':
                return redirect(url_for('home.error_page',code=1003))

        res=None
        is_ready=None
        if task_id:
            is_ready=task.tasks.get_comments.AsyncResult(task_id).ready()
            if is_ready:
                res=task.tasks.get_comments.AsyncResult(task_id).get()
                if res == -1:
                    return redirect(url_for('home.error_page',code=1001))
        elif target_id:
            is_ready=task.tasks.get_comments.AsyncResult(ttid).ready()
            if is_ready:
                res=task.tasks.get_comments.AsyncResult(ttid).get()
                if res == -1:
                    return redirect(url_for('home.error_page',code=1001))
                tmp=res['comments'][target_id]
                res={}
                res['comments']=tmp
                del res['comments'][0]

        return render_template('user/comment_with_you.html',is_ready=is_ready,res=res,url=request.url,task_id=task_id)


@mod.route('/share' ,methods=['GET'])
def share():
    status="搜寻回忆你与TA的评论，A Taste Of Funny In X2 -- http://www.227777777.com"

    client=model.weibo.APIClient(app_key=app.config['SINA_WEIBO_KEY'], app_secret=app.config['SINA_WEIBO_SECRET'], redirect_uri=url_for('home.sina_callback',_external=True))
    client.set_access_token(session['token'] , session['expires_in'])
    client.post.statuses__update(status=status)
    return redirect("http://weibo.com")

@mod.route('/records',methods=['GET'])
def records():
    if 'login_user' in session:
        records=None
        if session['login_user']['sid'] == '1710252570':
            records=model.home.get_records()
        else:
            records=model.home.get_records(session['login_user']['_id'])
        newr=[]
        for r in records:
            ltime=time.localtime(r['create'])
            r['create']=time.strftime('%Y-%m-%d %H:%M',ltime)
            if (int(time.time()) - int(time.mktime(ltime))) > 24*3600:
                r['expire']=True
            else:
                r['expire']=False
            
            if r['type'] == '1':
                r['is_done']=task.tasks.analyze_comment.AsyncResult(r['task_id']).ready()
            else:
                r['is_done']=task.tasks.get_comments.AsyncResult(r['task_id']).ready()
            newr.append(r)

    	return render_template('user/records.html',records=newr)



@mod.route('/register' ,methods=['GET','POST'])
def register():
    if 'login_user' in session:
        return redirect(url_for('home.index'))

    data={}
    error={}
    if request.method == 'POST':
        data['email']=request.form['email']
        data['name']=request.form['name']
        data['password1']=request.form['password1']
        data['password2']=request.form['password2']

        if not data['email']:
            error['email']=u'please fill in you email'
        elif not data['name']:
            error['name']=u'please fill in you name'
        elif not data['password1'] or not data['password2']:
            error['password1']=error['password2']=u'please fill in your password'
        elif data['password1'] != data['password2']:
            error['password1']=error['password2']=u'two passwords are not same'

        user=model.user.get_user_by_email(data['email'])
        if user.count() > 0:
            error['email']="this email has been taken"

        if not error:
            data['password']=data['password1']
            del data['password1']
            del data['password2']
            userid=model.user.new_user(data)
            return redirect(url_for('user.login',where='new_register'))

    return render_template('user/register.html',data=data,error=error)

@mod.route('/login' ,methods=['GET','POST'])
def login():
    if 'login_user' in session:
        return redirect(url_for('home.index'))

    extra={}
    error={}
    data={}

    extra['where']=request.args.get('where')

    if request.method == 'POST':
        data['email']=request.form['email']
        data['password']=request.form['password']

        if not data['email'] or not data['password']:
            error['all']="fill in you email and password"
        else:
            user=model.user.get_user_by_email(data['email'])
            user=user[0]
            if not user:
                error['all']="no such user :" + data['email']
            elif user['password'] != data['password']:
                error['all']="password error"
            else:
                session['login_user']=user
                return redirect(url_for('home.index'))

    return render_template('user/login.html',data=data,error=error,extra=extra)

@mod.route('/logout' ,methods=['GET','POST'])
def logout():
    session.pop('login_user',None)
    return redirect(url_for('home.index'))
