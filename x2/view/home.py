#coding=utf-8
from flask import Blueprint , session, render_template,url_for,redirect,request,g
from x2 import app
from x2 import model
import urllib2,time

SINA_KEY    =app.config['SINA_WEIBO_KEY']
SINA_SEC    =app.config['SINA_WEIBO_SECRET']


mod=Blueprint('home' ,__name__)

@mod.route('/' ,methods=['GET'])
def index():
    r_cnt=model.home.get_records().count()
    u_cnt=model.user.get_users().count()
    e_cnt=model.event.get_my_event().count()
    return render_template('index.html',u_cnt=u_cnt,r_cnt=r_cnt,e_cnt=e_cnt)

@mod.route('/sina_url',methods=['GET'])
def sina_url():
    client = model.weibo.APIClient(app_key=SINA_KEY, \
                                   app_secret=SINA_SEC, \
                                   redirect_uri=url_for('home.sina_callback',_external=True)
                                   )
    url = client.get_authorize_url()
    return  redirect(url)

@mod.route('/sina_callback',methods=['GET'])
def sina_callback():
    if request.method == 'GET':
        code=request.args.get('code')
        client=None
        r=None
        try:
            client = model.weibo.APIClient(app_key=SINA_KEY, \
                                       app_secret=SINA_SEC, \
                                       redirect_uri=url_for('home.sina_callback',_external=True)
                                       )
            r = client.request_access_token(code)
            #r={'access_token': u'2.00GLHHQCHhjcCC15c5478039QUJayC', 'remind_in': u'85176', 'expires_in': 1338863087, 'uid': u'2070382932'}
            #access_token = r.access_token 
            #expires_in = r.expires_in 
            client.set_access_token(r.access_token, r.expires_in)
        except model.weibo.APIError as apierr:
            print str(apierr)
            return redirect(url_for('home.error_page'))
        except urllib2.HTTPError as err:
            print str(err)   # 记录HTTP状态码
            print err.read() # 同时记录错误代码
            return redirect(url_for('home.error_page'))
        
        #uid=None
        #获取用户在微薄的个人信息
        #try:
        #    uid=client.get.account__get_uid(access_token=access_token)
        #except model.weibo.APIError as apierr:
        #    print str(apierr)
        #except urllib2.HTTPError as err:
        #    print str(err)   # 记录HTTP状态码
        #    print err.read() # 同时记录错误代码

        userinfo=None
        try:
            userinfo=client.get.users__show(uid=r.uid)
        except model.weibo.APIError as apierr:
            print str(apierr)
            return redirect(url_for('home.error_page'))
        except urllib2.HTTPError as err:
            print str(err)   # 记录HTTP状态码
            print err.read() # 同时记录错误代码
            return redirect(url_for('home.error_page'))

        #保存用户的token和expire_time
        #并且update或者insert一个用户信息
        user=model.home.save_sina_access_token(r,userinfo)
        if user:
            session['login_user']=user
            session['expires_in']=r.expires_in
            session['token']=r.access_token
            app.config['permanent_session_lifetime']= int(r.remind_in) - 120

        return redirect(url_for('home.index'))


@mod.route('/cancel_sina_callback',methods=['GET'])
def cancel_sina_callback():
    return "cancel it...."


@mod.route('/error',methods=['GET'])
def error_page():
    '''
    1001:task过期
    '''
    code=request.args.get('code',0)
    return render_template('error.html',code=code)

@mod.route('/tweet',methods=['GET'])
def tweet():
    if 'login_user' not in session:
        return "please login in"

    if request.method == 'GET':
        uid=request.args.get('uid','')
        mid=request.args.get('id','')

        if uid and mid:
            client=model.weibo.APIClient(app_key=app.config['SINA_WEIBO_KEY'], app_secret=app.config['SINA_WEIBO_SECRET'], redirect_uri=url_for('home.sina_callback',_external=True))
            client.set_access_token(session['token'] , session['expires_in'])
            try:
                midd=client.get.statuses__querymid(id=mid,type=1)
                midd=midd['mid']
            except model.weibo.APIError as apierr:
                print str(apierr)
            except urllib2.HTTPError as err:
                print str(err)   # 记录HTTP状态码
                print err.read() # 同时记录错误代码
            return redirect("http://weibo.com/" + str(uid) + "/" + str(midd))

    return render_template('error.html')
