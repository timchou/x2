#coding=utf-8
from __future__ import division
from flask import Blueprint ,request ,render_template ,session ,redirect ,url_for,jsonify
from x2 import model,app,celery,task
import urllib2,urllib,datetime,time

mod=Blueprint('event' ,__name__)

@mod.route('/event' ,methods=['GET'])
@mod.route('/event/index' ,methods=['GET'])
def index():
    if 'login_user' not in session:
        return redirect(url_for('home.index'))
    
    events=None
    if session['login_user']['sid'] == '1710252570':
        events=model.event.get_my_event()
    else:
        events=model.event.get_my_event(session['login_user']['_id'])
    return render_template('event/index.html',events=events)

@mod.route('/create_event' ,methods=['GET','POST'])
def create_event():
    if 'login_user' not in session:
        return redirect(url_for('home.index'))

    error=None
    event=None
    doc={}
    if request.method == 'POST':

        start_date=request.form['start_date']
        start_time=request.form['start_time']
        where=request.form['where']
        detail=request.form['detail']

        if not start_date:
            error="empty start date"
        elif not start_time:
            error="empty start time"
        elif not where:
            error="empty event place"
        elif not detail:
            error="empty event detail"

        if not error:
            doc={
                'uid':session['login_user']['_id'],
                'sd':start_date,
                'st':start_time,
                'w':where,
                'd':detail
                }
            event_id=model.event.save_event(doc)
            if event_id:
                return redirect(url_for('event.event',event_id=event_id))
    return render_template('event/create_event.html',error=error ,event=event,data=doc)


@mod.route('/event/<event_id>' ,methods=['GET','POST'])
def event(event_id):

    if request.method == 'POST':
        name=request.form['name']
        if event_id and name:
            model.event.add_guy_to_event(event_id,name)
            return redirect(url_for('event.event',event_id=event_id))

    if request.method == 'GET':
        event=None
        if event_id:
            event=model.event.get_event(event_id)
            user=model.user.get_user_by_userid(event['uid'])
        return render_template('event/event.html' ,event=event,cuser=user)

